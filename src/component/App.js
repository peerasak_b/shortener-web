import "../style/App.css";
import InputShortener from "../shared/input-shortener";
import LinkResult from "../shared/link-result";
import { useState } from "react";

function App() {
    const [inputValue, setInputValue] = useState("");
    return (
        <div className="container">
            <InputShortener setInputValue={setInputValue} />
            <LinkResult inputValue={inputValue} />
        </div>
    );
}

export default App;
